document.addEventListener("DOMContentLoaded", function() {
    const now = new Date();
    const workingDay = now.getDay() !== 0 && now.getDay() !== 6;
    const openingHours = now.getHours() >= 8 && now.getHours() < 17;
    if (openingHours) {
        const escalationBlock = document.querySelectorAll('.escalation-block');
        escalationBlock.forEach(function(el) {
            el.classList.add('active')
        })
    }
    let esc_app = document.getElementById('app-block');
    let vvaa_app = document.getElementById('vvaa-app');
    localStorage.removeItem('category');
    localStorage.removeItem('subcategory');
    localStorage.removeItem('searchsubject');
    localStorage.removeItem('faq');

    if (window.navigator.userAgent.indexOf("Mac") != -1) {
        esc_app.href= 'https://apps.apple.com/nl/app/vvaa-app/id1372755258';
        vvaa_app.href= 'https://apps.apple.com/nl/app/vvaa-app/id1372755258';
    }
    else {
        esc_app.href = 'https://play.google.com/store/apps/details?id=nl.vvaa.app&hl=nl';
        vvaa_app.href = 'https://play.google.com/store/apps/details?id=nl.vvaa.app&hl=nl';
    }

    function getUrlParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
    }

    let searchTerm = getUrlParameter('addsearch');
    if (searchTerm) {
        let searchBar = document.getElementById('searchbar');
        searchBar.value = searchTerm;
        let searchBarValue = searchBar.value
        search(searchBarValue);
    }

    fetch('https://api-cms.digitalcx.com/classifications/33/categories?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&includefaqs=false')
    .then(response => { response.json().then(
        function(data) {
            let result = data.result;
            for (var i = 0; i < result.length; i++) {
                let categories = document.getElementById('categories');
                let subcategoryLists = document.getElementById('subcat-lists');
                let category = data.result[i];
                let imgName = category.name.toLowerCase();
                imgName = imgName.split(' ').join('');
                imgName = imgName.split('&').join('-');
                let categoryUrl = category.name.toLowerCase();
                if (categoryUrl) {
                    categoryUrl = categoryUrl.split(' & ').join('-');
                    categoryUrl = categoryUrl.split(' ').join('-');
                }
                let sessionId = data.sessionId;
                localStorage.setItem('cx-session', sessionId);
                let categoryHtml =
                    '<div class="col-xs-6 col-md-4">' +
                    '<div class="category tree tree-category" data-link="' + categoryUrl + '" data-id="' + category.id + '" onclick="selectCategory(this)">' +
                        '<img src="https://www.vvaa.nl/-/media/service/icons/gray/' + imgName + '.svg" />' +
                        '<img class="active-icon" src="https://www.vvaa.nl/-/media/service/icons/white/' + imgName + '.svg" />' +
                        '<span>' + category.name + '</span>' +
                    '</div>' +
                '</div>'
                let subcategoryHtml = '';
                if (category.subCategories.length) {
                    for (var j = 0; j < category.subCategories.length; j++) {
                        let subcategory = category.subCategories[j];
                        let subCategoryUrl = subcategory.name.toLowerCase();
                        subCategoryUrl = subCategoryUrl.split(' & ').join('-');
                        subCategoryUrl = subCategoryUrl.split(' ').join('-');
                        subcategoryHtml += '<li class="subcategory tree tree-subcategory" data-link="' + subCategoryUrl + '" onclick="selectSubcategory(this)" data-id=' + subcategory.id + '>' + subcategory.name + '</li>';
                    }
                    let subcategoryWrapperHtml =
                        '<ul class="subcat-list" data-id=' + category.id + '>' + subcategoryHtml +
                        '</ul>';
                    subcategoryLists.insertAdjacentHTML('beforeend', subcategoryWrapperHtml);
                }
                if (category.name !== 'Default') {
                    categories.insertAdjacentHTML('beforeend', categoryHtml);
                }
                document.getElementById('loader').style.display = 'none';
            }
            let deeplink = getUrlParameter('q');
            if (deeplink) {
                deeplink = deeplink.split('/');
                let categoryFirstChar = deeplink[0].charAt(0);
                const category = document.querySelectorAll('.category');
                category.forEach(function(el) {
                    let deeplinkSplit;
                    if (categoryFirstChar === '0' ||
                    categoryFirstChar === '1' ||
                    categoryFirstChar === '2' ||
                    categoryFirstChar === '3' ||
                    categoryFirstChar === '4' ||
                    categoryFirstChar === '5' ||
                    categoryFirstChar === '6' ||
                    categoryFirstChar === '7' ||
                    categoryFirstChar === '8' ||
                    categoryFirstChar === '9') {
                        deeplinkSplit = deeplink[0].split('-');
                    }
                    if (el.dataset.link === deeplink[0]) {
                        el.dataset.deeplink = 'true';
                        el.click();
                        const subcatLists = document.querySelectorAll('.subcat-list');
                        subcatLists.forEach(function(ele) {
                            if (deeplink[1]) {
                                if (ele.dataset.id === el.dataset.id) {
                                    const subcategory = ele.querySelectorAll('.subcategory');
                                    subcategory.forEach(function(elem) {
                                        deeplink[1] = deeplink[1].toLowerCase();
                                        deeplink[1] = deeplink[1].split(' ').join('-');
                                        if (elem.dataset.link === deeplink[1]) {
                                            elem.dataset.deeplink = 'true';
                                            elem.click();
                                        }
                                    })
                                }
                            }
                        })
                    }
                    if (deeplinkSplit) {
                        if (deeplinkSplit[0] === el.dataset.id) {
                            el.dataset.deeplink = 'true';
                            el.click();
                            const subcatLists = document.querySelectorAll('.subcat-list');
                            subcatLists.forEach(function(ele) {
                                if (deeplink[1]) {
                                    if (ele.dataset.id === el.dataset.id) {
                                        const subcategory = ele.querySelectorAll('.subcategory');
                                        subcategory.forEach(function(elem) {
                                            deeplink[1] = deeplink[1].toLowerCase();
                                            deeplink[1] = deeplink[1].split(' ').join('-');
                                            if (elem.dataset.link === deeplink[1]) {
                                                elem.dataset.deeplink = 'true';
                                                elem.click();
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                })
            }
        });
    });
});

function selectCategory(e) {
    let subcategories = document.getElementById('subcategories');
    let faqs = document.getElementById('faqs');
    if (faqs !== null) {
        faqs.remove();
    }
    document.getElementById('faq-section').style.display = 'none';
    document.getElementById('question-section').style.display = 'none';
    document.getElementById('escalations').style.display = 'none';
    document.getElementById('self-service').style.display = 'block';
    document.getElementById('sos').style.display = 'block';
    const escalation_block = document.querySelectorAll('.escalation-block--tree');
    escalation_block.forEach(function(el) {
        el.style.display = 'none';
    })
    let id = e.dataset.id;
    let subcatLists = document.getElementById('subcat-lists').getElementsByTagName('ul');
    if (subcategories.getElementsByTagName("h3")[0]) {
        subcategories.getElementsByTagName("h3")[0].remove();
    }
    const category = document.querySelectorAll('.category');
    category.forEach(function(el) {
        el.classList.remove('active')
    })
    e.classList.add('active');
    let selectedCategory = e.getElementsByTagName("span")[0].innerHTML;
    let selectedCategoryhtml = '<h3>' + selectedCategory + '</h3>';
    localStorage.setItem('category', selectedCategory);
    subcategories.insertAdjacentHTML('afterbegin', selectedCategoryhtml);
    subcategories.style.display = "block";
    for (var i = 0; i < subcatLists.length; i++) {
        let list = subcatLists[i];
        list.style.display = "none";
        if (list.dataset.id === id) {
            list.style.display = "block";
        }
    }
    if (!e.dataset.deeplink) {
        let categoryUrl = e.dataset.link;
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
        newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?q=' + categoryUrl;
        window.history.pushState({ path: newurl }, '', newurl);
        scrollPageTo(document.getElementById('subcategories'), 700);
    }
    else {
        let params = window.location.search;
        params = params.split('/');
        if (params.length === 1) {
            scrollPageTo(document.getElementById('subcategories'), 700);
        }
    }
}

function selectSubcategory(e) {
    let id = e.dataset.id;
    let faqs = document.getElementById('faqs');
    if (faqs !== null) {
        faqs.remove();
    }
    let subCategoryTitle = e.innerHTML;
    localStorage.setItem('subcategory', subCategoryTitle);
    document.getElementById('self-service').style.display = 'block';
    document.getElementById('sos').style.display = 'block';
    document.getElementById('faq-section').style.display = 'none';
    const subcategory = document.querySelectorAll('.subcategory');
    subcategory.forEach(function(el) {
        el.classList.remove('active');
    })
    document.getElementById('escalations').style.display = 'none';
    const escalation_block = document.querySelectorAll('.escalation-block--tree');
    escalation_block.forEach(function(el) {
        el.style.display = 'none';
    })
    e.classList.add('active');
    if (!e.dataset.deeplink) {
        let subcategoryUrl = e.dataset.link;
        let params = window.location.search;
        params = params.split('/');
        newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + params[0] + '/' + subcategoryUrl;
        window.history.pushState({ path: newurl }, '', newurl);
    }
    let sessionId = localStorage.getItem('cx-session');
    fetch('https://api-cms.digitalcx.com/classifications/33/categories/' + id + '/faqs?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&session.id=' + sessionId)
    .then(response => { response.json().then(
        function(data) {
            let faqSection = document.getElementById('question-section');
            faqSection.style.display = "block";
            let faqHtml = '';
            let category = localStorage.getItem('category');
            category = category.split(' ').join('');
            let subcategory = localStorage.getItem('subcategory');
            subcategory = subcategory.split(' ').join('');
            let piwikAction  = category + '-' + subcategory;
            for (var i = 0; i < data.result.length; i++) {
                let faq = data.result[i];
                let faqUrl = faq.question.toLowerCase();
                faqUrl = faqUrl.split(' & ').join('-');
                faqUrl = faqUrl.split(' ').join('-');
                faqUrl = faqUrl.split('?').join('');
                faqHtml += '<li class="faq tree tree-faq" data-piwik="' + piwikAction + '" data-link="' + faqUrl + '" onclick="selectFaq(this)" data-id=' + faq.id + '>' + faq.question + '</li>';
            }
            let faqWrapperHtml =
                '<ul id="faqs">' + faqHtml + '</ul>';
            faqSection.insertAdjacentHTML('beforeend', faqWrapperHtml);
            let params = window.location.search;
            params = params.split('/');
            if (params[2]) {
                params[2] = params[2].toLowerCase();
                params[2] = params[2].split('%20').join('-');
                params[2] = params[2].replace('?', '');
                if (params[2] !== undefined) {
                    const faq = document.querySelectorAll('.faq');
                    faq.forEach(function(el) {
                        if (el.dataset.link === params[2]) {
                            el.click();
                        }
                    })
                }
                else {
                    scrollPageTo(document.getElementById('question-section'), 700);
                }
            }
            else {
                scrollPageTo(document.getElementById('question-section'), 700);
            }
        });
    });
}


function selectFaq(e) {
    let id = e.dataset.id;
    const faq = document.querySelectorAll('.faq');
    faq.forEach(function(el) {
        el.classList.remove('active');
    })
    e.classList.add('active');
    document.getElementById('escalations').style.display = 'none';
    const escalation_block = document.querySelectorAll('.escalation-block--tree');
    escalation_block.forEach(function(el) {
        el.style.display = 'none';
    })
    let params = window.location.search;
    params = params.split('/');
    let faqUrl = e.dataset.link;
    newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + params[0] + '/' + params[1] + '/' + faqUrl;
    window.history.pushState({ path: newurl }, '', newurl);
    let faqSection =  document.getElementById('faq-section');
    faqSection.style.display = 'block';
    faqSection.innerHTML = '<img src="https://www.vvaa.nl/-/media/service/icons/spinner.gif" id="faq-loader" style="display: block;" />';
    scrollPageTo(document.getElementById('faq-section'), 700);
    let sessionId = localStorage.getItem('cx-session');
    fetch('https://api-cms.digitalcx.com/faqs/' + id + '/output?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&classificationId=33&session.id=' + sessionId)
    .then(response => { response.json().then(
        function(data) {
            let question = data.outputs[0].faqQuestion;
            let answer = data.outputs[0].outputParts[0];
            let interactionId = data.interactionId;
            linksAndImages(answer, interactionId);
            let answerText = marked(answer.text);
            if (answerText.includes('javascript:')) {
                answerText = answerText.replace('target="_blank"', '');
                answerText = answerText.replace('target="blank"', '');
            }
            localStorage.setItem('faq', question);
            let answerHtml = '<div class="faq">' +
                '<h3>' + question + '</h3>' +
                '<p>' + answerText + '</p>' +
            '</div>' +
            '<div id="fulfilled">' +
            '<h4>Heeft dit antwoord geholpen?</h4>' +
            '<span class="button feedback-button feedback-yes" onclick="positiveFeedback(\'' + interactionId + '\', \'' + question + '\')" data-faq="' + question + '">Ja</span>' +
            '<span class="button feedback-button feedback-no" onclick="negativeFeedback(\'' + interactionId + '\', \'' + question + '\')" data-faq="' + question + '">Nee, ik wil graag contact</span>' +
            '<span class="feedback-thanks">Bedankt voor uw feedback!</span>'
            '</div>';
            faqSection.insertAdjacentHTML('beforeend', answerHtml);
            let faqLoader = document.getElementById('faq-loader');
            faqLoader.style.display = 'none';
            document.getElementById('self-service').style.display = 'none';
            document.getElementById('sos').style.display = 'none';
            let escalationString = data.outputs[0].outputParts[0].metadata.contactEscalationNew;
            let escalations = JSON.parse(escalationString);
            if (escalations.chat || escalations.livechat) {
                document.getElementById('esc_chat').style.display = 'block';
            }
            if (escalations.bellen) {
                document.getElementById('esc_call_ls').style.display = 'block';
            }
            if (escalations.bellenss || escalations.telefoonls || escalations.telefoonss) {
                document.getElementById('esc_call_ss').style.display = 'block';
            }
            if (escalations.bericht) {
                document.getElementById('esc_messenger').style.display = 'block';
            }
            if (escalations.app) {
                document.getElementById('esc_app').style.display = 'block';
            }
            if (escalations.whatsapp) {
                document.getElementById('esc_whatsapp').style.display = 'block';
            }
            if (escalations.mijnvvaa) {
                document.getElementById('esc_mijnvvaa').style.display = 'block';
            }
        });
    });
}

function searchSuggest(e) {
    let sessionId = localStorage.getItem('cx-session');
    let searchTerm = e.value;
    if (searchTerm.length > 4) {
        fetch('https://api-cms.digitalcx.com/faqs?customerkey=VVAA&projectkey=VVAA&searchText=' + searchTerm + '&classificationid=33&culture=nl&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&max=5&text=' + searchTerm + '&session.id=' + sessionId)
        .then(response => { response.json().then(
            function(data) {
                let sessionId = data.sessionId;
                localStorage.setItem('cx-session', sessionId);
                let suggestions = data.result.matches;
                const overlay = document.getElementById('overlay');
                const search = document.getElementById('service-search');
                if (suggestions.length) {
                    overlay.classList.add('active');
                    let searchBar = document.getElementById('searchbar');
                    let submit = document.getElementById('submit');
                    searchBar.classList.add('active');
                    submit.classList.add('active');
                    let modifiedSuggestions = [];
                    for (var i = 0; i < suggestions.length; i++) {
                        let result = suggestions[i].question;
                        let reg = new RegExp(searchTerm, 'gi');
                        let final_str = result.replace(reg, function(str) {return '<i>'+str+'</i>'});
                        modifiedSuggestions.push(final_str);
                    }
                    let suggestionsHtml = '';
                    for (var i = 0; i < modifiedSuggestions.length; i++) {
                        let suggestion = modifiedSuggestions[i];
                        let cleanSuggestion = suggestion.split('<i>').join('');
                        cleanSuggestion = cleanSuggestion.split('</i>').join('');
                        suggestionsHtml += '<li class="suggestion" onclick="selectSuggestion(\'' + cleanSuggestion + '\')"><img src="https://www.vvaa.nl/-/media/service/icons/orange/chevron.svg" /><p class="suggestion-inner" data-term="' + cleanSuggestion + '">' + suggestion + '</p></li>';
                    }
                    let suggestHtmlWrapper = '';
                    let searchItem = '<li class="search-item" onclick="selectSuggestion(\'' + searchTerm + '\')"><img src="https://www.vvaa.nl/-/media/service/icons/orange/search.svg" /><p  class="suggestion-inner" data-term="' + searchTerm + '">Zoeken naar <b>' + searchTerm + '</p></b></li>';
                    suggestHtmlWrapper = '<ul id="suggestions">' + suggestionsHtml + searchItem + '</ul>';
                    const suggestionsDiv = document.getElementById('suggestions');
                    if (suggestionsDiv !== null) {
                        suggestionsDiv.remove();
                    }
                    search.insertAdjacentHTML('beforeend', suggestHtmlWrapper);
                }
                else {
                    overlay.classList.remove('active');
                    const suggestionsDiv = document.getElementById('suggestions');
                    if (suggestionsDiv !== null) {
                        suggestionsDiv.remove();
                    }
                }
            });
        });
    }
    if (e.value.length < 2) {
        const suggestions = document.getElementById('suggestions');
        if (suggestions) {
            suggestions.remove();
        }
        let searchBar = document.getElementById('searchbar');
        let submit = document.getElementById('submit');
        searchBar.classList.remove('active');
        submit.classList.remove('active');
    }

}

function selectSuggestion(term) {
    const overlay = document.getElementById('overlay');
    overlay.classList.remove('active');
    const suggestions = document.getElementById('suggestions');
    suggestions.remove();
    search(term);
    localStorage.setItem('faq', term);
    let searchBar = document.getElementById('searchbar');
    let submit = document.getElementById('submit');
    searchBar.classList.remove('active');
    submit.classList.remove('active');
}

function closeSuggest() {
    const overlay = document.getElementById('overlay');
    overlay.classList.remove('active');
    const suggestions = document.getElementById('suggestions');
    if (suggestions) {
        suggestions.remove();
    }
    let searchBar = document.getElementById('searchbar');
    let submit = document.getElementById('submit');
    searchBar.classList.remove('active');
    submit.classList.remove('active');
}

function checkInput(e) {
    let searchResults = document.getElementById('search-results');
    let searchResultsHead = searchResults.getElementsByTagName('h4')[0];
    let escalationsSearch = document.getElementById('escalations-search');
    escalationsSearch.style.display = 'none';
    let feedbackSearch = document.getElementById('feedback-search');
    feedbackSearch.style.display = 'none';
    if (event.keyCode === 27 && searchResultsHead) {
        searchResults.innerHTML = '';
        searchResults.style.display = 'none';
    }
    if (event.keyCode === 8 && searchResultsHead) {
        searchResults.innerHTML = '';
        searchResults.style.display = 'none';
    }
}

function search(e) {
    localStorage.removeItem('prev-path');
    const overlay = document.getElementById('overlay');
    overlay.classList.remove('active');
    let escalationsSearch = document.getElementById('escalations-search');
    escalationsSearch.style.display = 'none';
    let feedbackSearch = document.getElementById('feedback-search');
    feedbackSearch.style.display = 'none';
    let searchbar = document.getElementById('searchbar');
    localStorage.removeItem('category');
    localStorage.removeItem('subcategory');
    localStorage.removeItem('searchsubject');
    localStorage.removeItem('faq');
    let search =  document.getElementById('service-search');
    search.dataset.term = searchbar.value;
    let input;
    if (e.bubbles) {
        e.preventDefault();
        input = searchbar.value;
    }
    else {
        input = e;
    }
    // let inputLowerCase = input.toLowerCase();
    // inputLowerCase = inputLowerCase.trim();
    // if (inputLowerCase === 'wijzigen' ||
    //     inputLowerCase === 'adreswijziging' ||
    //     inputLowerCase === 'adres wijzigen' ||
    //     inputLowerCase === 'adres wijziging' ||
    //     inputLowerCase === 'rekeningnummer wijzigen' ||
    //     inputLowerCase === 'wachtwoord wijzigen' ||
    //     inputLowerCase === 'verzekering wijzigen' ||
    //     inputLowerCase === 'aanpassen' ||
    //     inputLowerCase === 'veranderen' ||
    //     inputLowerCase === 'wijziging doorgeven') {
    //     triggerBot('wijzigen_bot');
    // }
    // if (inputLowerCase === "schade melden" ||
    //     inputLowerCase === 'schademelden' ||
    //     inputLowerCase === 'schademelding' ||
    //     inputLowerCase === 'schade melding' ||
    //     inputLowerCase === 'autoschade melden' ||
    //     inputLowerCase === 'auto schade melden' ||
    //     inputLowerCase === 'schade melden aanrijding' ||
    //     inputLowerCase === 'ruitschade' ||
    //     inputLowerCase === 'letselschade' ||
    //     inputLowerCase === 'schade formulier' ||
    //     inputLowerCase === 'schade doorgeven' ||
    //     inputLowerCase === 'schades' ||
    //     inputLowerCase === 'autoruitschade' ||
    //     inputLowerCase === 'autoschade' ||
    //     inputLowerCase === 'waterschade' ||
    //     inputLowerCase === 'schade auto' ||
    //     inputLowerCase === 'kapotte auto' ||
    //     inputLowerCase === 'auto kapot' ||
    //     inputLowerCase === 'ruit kapot' ||
    //     inputLowerCase === 'kapotte ruit' ||
    //     inputLowerCase === 'reisschade ' ||
    //     inputLowerCase === 'koffer kapot') {
    //     triggerBot('schade_melden_bot');
    // }
    // if (inputLowerCase === "factuur betalen" ||
    //     inputLowerCase === "facturen betalen" ||
    //     inputLowerCase === 'nota betalen' ||
    //     inputLowerCase === 'rekening betalen' ||
    //     inputLowerCase === 'premie betalen' ||
    //     inputLowerCase === 'faktuur betalen' ||
    //     inputLowerCase === 'uitstel van betaling' ||
    //     inputLowerCase === 'betalings regeling' ||
    //     inputLowerCase === 'betalingsregelingt' ||
    //     inputLowerCase === 'betalen uitstellen') {
    //     triggerBot('factuur_betalen');
    // }
    localStorage.setItem('category', input);
    const suggestions = document.getElementById('suggestions');
    if (suggestions) {
        suggestions.remove();
    }
    let sessionId = localStorage.getItem('cx-session');
    if (input.length > 2) {
        fetch('https://api-cms.digitalcx.com/ask?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&maxfaqs=10&q=' + input + '&session.id=' + sessionId)
        .then(response => { response.json().then(
            function(data) {
                let searchResults = document.getElementById('search-results');
                searchResults.innerHTML = '';
                let sessionId = data.sessionId;
                let interactionId = data.interactionId;
                localStorage.setItem('cx-session', sessionId);
                let dialogPath = data.outputs[0].dialogPath;
                let prevPath = localStorage.getItem('prev-path');
                if (!prevPath) {
                    localStorage.setItem('prev-path', dialogPath);
                }
                let termHtml = '<h4>' + input + '</h4>';
                searchResults.insertAdjacentHTML('afterbegin', termHtml);
                let answers = data.outputs[0].outputParts;
                for (var i = 0; i < answers.length; i++) {
                    let answer = answers[i];
                    linksAndImages(answer, interactionId);
                    let answerText = marked(answer.text);
                    if (answerText.includes('javascript:')) {
                        answerText = answerText.replace('target="_blank"', '');
                        answerText = answerText.replace('target="blank"', '');
                    }
                    answerText = answerText.split('%{DialogOption(').join('');
                    answerText = answerText.split(')}').join('');
                    if (dialogPath && !dialogPath.includes('!')) {
                        answerText = answerText.split('<li>').join('<li class="dialog-option" data-term="' + input + '" data-path="' + dialogPath +  '" onclick="searchOption(this)">');
                    }
                    let answerHtml = '<p>' + answerText + '</p>';
                    let fullfilledHtml = '<div id="fulfilled-search">' +
                    '<h4>Heeft dit antwoord geholpen?</h4>' +
                    '<span class="button feedback-button feedback-yes feedback-search" onclick="positiveFeedbackSearch(\'' + interactionId + '\', \'' + input + '\')" data-faq="' + input + '">Ja</span>' +
                    '<span class="button feedback-button feedback-no feedback-search" onclick="negativeFeedbackSearch(\'' + interactionId + '\', \'' + input + '\')" data-faq="' + input + '">Nee, ik wil graag contact</span>' +
                    '<span class="feedback-thanks">Bedankt voor uw feedback!</span>'
                    '</div>';
                    searchResults.insertAdjacentHTML('beforeend', answerHtml);
                    searchResults.style.display = 'block';

                    let feedbackSearch = document.getElementById('feedback-search');
                    feedbackSearch.style.display = 'none';
                    if (!answer.dialogOptions.length) {
                        feedbackSearch.style.display = 'block';
                        feedbackSearch.innerHTML = '';
                        feedbackSearch.insertAdjacentHTML('beforeend', fullfilledHtml);
                    }
                }
            });
        });
    }
}

function searchOption(el) {
    let value = el.innerHTML;
    let path = el.dataset.path;
    let term = el.dataset.term;
    path = encodeURIComponent(path);
    let searchResults = document.getElementById('search-results');
    searchResults.innerHTML = '';
    let escalationsSearch = document.getElementById('escalations-search');
    escalationsSearch.style.display = 'none';
    let sessionId = localStorage.getItem('cx-session');
    fetch('https://api-cms.digitalcx.com/ask?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&maxfaqs=10&q=' + value + '&session.id=' + sessionId)
    .then(response => { response.json().then(
        function(data) {
            let dialogPath = data.outputs[0].dialogPath;
            let interactionId = data.interactionId;
            let prevTerm = localStorage.getItem('prev-term');
            if (prevTerm) {
                term = prevTerm;
            }
            else {
                localStorage.setItem('prev-term', value);
            }
            let backLinkHtml = '<span class="back-link" data-term="' + term + '" data-path="' + path + '" onClick="backOption(this)"><img src="https://www.vvaa.nl/-/media/service/icons/gray/chevron.svg" /> Ga terug</span>'
            let termHtml = '<h4>' + value + '</h4>';
            localStorage.setItem('searchsubject', value);
            searchResults.insertAdjacentHTML('afterbegin', termHtml);
            searchResults.insertAdjacentHTML('afterbegin', backLinkHtml);
            let answers = data.outputs[0].outputParts;
            for (var i = 0; i < answers.length; i++) {
                let answer = answers[i];
                linksAndImages(answer, interactionId);
                let answerText = marked(answer.text);
                if (answerText.includes('javascript:')) {
                    answerText = answerText.replace('target="_blank"', '');
                    answerText = answerText.replace('target="blank"', '');
                }
                answerText = answerText.split('%{DialogOption(').join('');
                answerText = answerText.split(')}').join('');
                if (dialogPath && !dialogPath.includes('!')) {
                    answerText = answerText.split('<li>').join('<li class="dialog-option" data-term="' + term + '" data-path="' + dialogPath +  '" onclick="searchOption(this)">');
                }
                let answerHtml = '<p>' + answerText + '</p>';
                searchResults.insertAdjacentHTML('beforeend', answerHtml);
                searchResults.style.display = 'block';
                let feedbackSearch = document.getElementById('feedback-search');
                let fullfilledHtml = '<div id="fulfilled-search">' +
                '<h4>Heeft dit antwoord geholpen?</h4>' +
                '<span class="button feedback-button feedback-yes feedback-search" onclick="positiveFeedbackSearch(\'' + interactionId + '\', \'' + value + '\')" data-faq="' + value + '">Ja</span>' +
                '<span class="button feedback-button feedback-no feedback-search" onclick="negativeFeedbackSearch(\'' + interactionId + '\', \'' + value + '\')" data-faq="' + value + '">Nee, ik wil graag contact</span>' +
                '<span class="feedback-thanks">Bedankt voor uw feedback!</span>'
                '</div>';
                feedbackSearch.style.display = 'none';
                if (!answer.dialogOptions.length) {
                    feedbackSearch.style.display = 'block';
                    feedbackSearch.innerHTML = '';
                    feedbackSearch.insertAdjacentHTML('beforeend', fullfilledHtml);
                }
            }
        });
    });
}

function backOption(el, prev) {
    let path = el.dataset.path;
    let term = el.dataset.term;
    let sessionId = localStorage.getItem('cx-session');
    let searchResults = document.getElementById('search-results');
    let escalationsSearch = document.getElementById('escalations-search');
    escalationsSearch.style.display = 'none';
    searchResults.innerHTML = '';
    fetch('https://api-cms.digitalcx.com/dialogstep?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&path=' + path + '&session.id=' + sessionId)
    .then(response => { response.json().then(
        function(data) {
            let dialogPath = data.outputs[0].dialogPath;
            let interactionId = data.interactionId;
            let prevPath = localStorage.getItem('prev-path');
            prevPath = encodeURIComponent(prevPath);
            let prevTerm = localStorage.getItem('prev-term');
            localStorage.setItem('prev-term', term);
            if (!dialogPath.includes('/')) {
                localStorage.removeItem('prev-path');
                localStorage.removeItem('prev-term');
                let category = localStorage.getItem('category');
                search(category);
            }
            let backLinkHtml = '';
            if (!prev) {
                backLinkHtml = '<span class="back-link" data-term="' + term + '" data-path="' + prevPath + '" onClick="backOption(this, true)"><img src="https://www.vvaa.nl/-/media/service/icons/gray/chevron.svg" /> Ga terug</span>'
            }
            let termHtml = '<h4>' + term + '</h4>';
            localStorage.setItem('searchsubject', term);
            searchResults.insertAdjacentHTML('afterbegin', termHtml);
            searchResults.insertAdjacentHTML('afterbegin', backLinkHtml);
            let answers = data.outputs[0].outputParts;
            for (var i = 0; i < answers.length; i++) {
                let answer = answers[i];
                linksAndImages(answer, interactionId);
                let answerText = marked(answer.text);
                if (answerText.includes('javascript:')) {
                    answerText = answerText.replace('target="_blank"', '');
                    answerText = answerText.replace('target="blank"', '');
                }
                answerText = answerText.split('%{DialogOption(').join('');
                answerText = answerText.split(')}').join('');
                if (dialogPath && !dialogPath.includes('!')) {
                    answerText = answerText.split('<li>').join('<li class="dialog-option" data-term="' + term + '" data-path="' + dialogPath +  '" onclick="searchOption(this)">');
                }
                let answerHtml = '<p>' + answerText + '</p>';
                searchResults.insertAdjacentHTML('beforeend', answerHtml);
                searchResults.style.display = 'block';
                let feedbackSearch = document.getElementById('feedback-search');
                let fullfilledHtml = '<div id="fulfilled-search">' +
                '<h4>Heeft dit antwoord geholpen?</h4>' +
                '<span class="button feedback-button feedback-yes feedback-search" onclick="positiveFeedbackSearch(\'' + interactionId + '\', \'' + term + '\')" data-faq="' + term + '">Ja</span>' +
                '<span class="button feedback-button feedback-no feedback-search" onclick="negativeFeedbackSearch(\'' + interactionId + '\', \'' + term + '\')" data-faq="' + term + '">Nee, ik wil graag contact</span>' +
                '<span class="feedback-thanks">Bedankt voor uw feedback!</span>'
                '</div>';
                feedbackSearch.style.display = 'none';
                if (!answer.dialogOptions.length) {
                    feedbackSearch.style.display = 'block';
                    feedbackSearch.innerHTML = '';
                    feedbackSearch.insertAdjacentHTML('beforeend', fullfilledHtml);
                }
            }
        });
    });
}

function showEscalations() {
    let escalations = document.getElementById('escalations');
    escalations.style.display = 'block';
    const escalationBlock = document.querySelectorAll('.escalation-block');
    let category = localStorage.getItem('category');
    category = category.split(' ').join('');
    let subcategory = localStorage.getItem('subcategory');
    subcategory = subcategory.split(' ').join('');
    let faq = localStorage.getItem('faq');
    let term = document.getElementById('searchbar').value;
    escalationBlock.forEach(function(el) {
        el.querySelector('.block-inner').dataset.piwik = el.querySelector('.block-inner').dataset.type + ' - ' + category + '-' + subcategory;
        el.querySelector('.block-inner').dataset.faq = faq;
    })
    scrollPageTo(document.getElementById('escalations'), 700);
}

function showEscalationsSearch() {
    let escalations = document.getElementById('escalations-search');
    escalations.style.display = 'block';
    const escalationBlock = document.querySelectorAll('.escalation-block--search');
    let faq = localStorage.getItem('faq');
    let term;
    if (faq) {
        term = faq;
    }
    else {
        term = document.getElementById('searchbar').value;
    }
    escalationBlock.forEach(function(el) {
        el.querySelector('.block-inner').dataset.piwik = el.querySelector('.block-inner').dataset.type;
        el.querySelector('.block-inner').dataset.term = term;

    })
    scrollPageTo(document.getElementById('escalations'), 700);
}

function showPhoneNumber(el) {
    el.classList.add('active-phone');
}

function positiveFeedback(interactionId, question) {
    const data = {
        "originInteractionId": interactionId,
        "score": 1,
        "label": question,
    }
    fetch('https://api-cms.digitalcx.com/feedback?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        let fulfilled = document.getElementById('fulfilled');
        fulfilled.getElementsByTagName('h4')[0].style.display = 'none';
        fulfilled.getElementsByTagName('span')[0].style.display = 'none';
        fulfilled.getElementsByTagName('span')[1].style.display = 'none';
        fulfilled.getElementsByTagName('span')[2].style.display = 'block';
    });
}

function negativeFeedback(interactionId, question) {
    showEscalations();
    const data = {
        "originInteractionId": interactionId,
        "score": -1,
        "label": question,
    }
    fetch('https://api-cms.digitalcx.com/feedback?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
    });
}

function positiveFeedbackSearch(interactionId, question) {
    const data = {
        "originInteractionId": interactionId,
        "score": 1,
        "label": question,
    }
    fetch('https://api-cms.digitalcx.com/feedback?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        let fulfilled = document.getElementById('fulfilled-search');
        fulfilled.getElementsByTagName('h4')[0].style.display = 'none';
        fulfilled.getElementsByTagName('span')[0].style.display = 'none';
        fulfilled.getElementsByTagName('span')[1].style.display = 'none';
        fulfilled.getElementsByTagName('span')[2].style.display = 'block';
    });
}

function negativeFeedbackSearch(interactionId, question) {
    showEscalationsSearch();
    const data = {
        "originInteractionId": interactionId,
        "score": -1,
        "label": question,
    }
    fetch('https://api-cms.digitalcx.com/feedback?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
    }).then((resp) => {return resp.text();
    }).then(function(data) {
        console.log('success');
    });
}

// UTILS
let scrollPageTo = (to, duration=500) => {
    const easeInOutQuad = function (t, b, c, d) {
        t /= d/2;
        if (t < 1) return c/2*t*t + b;
            t--;
        return -c/2 * (t*(t-2) - 1) + b;
    };
    return new Promise((resolve, reject) => {
        const element = document.scrollingElement;
        if (typeof to === 'string') {
            to = document.querySelector(to) || reject();
        }
        if (typeof to !== 'number') {
            to = to.getBoundingClientRect().top + element.scrollTop;
        }
        let start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

        const animateScroll = function() {
            currentTime += increment;
            let val = easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if(currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
            else {
                resolve();
            }
        };
        animateScroll();
    });
}

function selectLink(e) {
    let interactionId = e.dataset.interaction;
    let url = e.href;
    let id = e.dataset.id;
    fetch('https://api-cms.digitalcx.com/linkclick?customerkey=VVAA&projectkey=VVAA&apikey=9dc18118-4f25-46c5-b071-da4132bc8903&culture=nl&classificationId=33&originInteractionId=' + interactionId + '&url=' + url + '&urlId=' + id)
    .then(response => { response.json().then(
        function(data) {
            console.log('success');
        });
    });
}

function linksAndImages(answer, interactionId) {
    if (answer.links.length) {
        for (var a = 0; a < answer.links.length; a++) {
            let iterator = a + 1;
            let link = answer.links[a];
            let url = link.url;
            let id = link.id;
            let linkTextToLowerCase = link.text.toLowerCase();
            let linkHTML = '<a href="' + url + '" target="_blank" data-id="' + link.id + '" data-interaction="' + interactionId + '" onclick="selectLink(this)">' + link.text + '</a>';
            let linkString = '%{Link(' + iterator + ')}';
            answer.text = answer.text.replace(linkString, linkHTML);
            if (answer.images.length) {
                for (var b = 0; b < answer.images.length; b++) {
                    let image = answer.images[b];
                    let iteratorImages = b + 1;
                    let imageHtml = '';
                    if (image.linkId) {
                        if (link.id === image.linkId) {
                            let imageHTML = '<a href="' + link.url + '" target="_blank"><img src="' + image.name + '" alt ="' + image.title + '"></img></a>';
                            let imageString = '%{Image(' + iteratorImages + ')}';
                            answer.text = answer.text.replace(imageString, imageHTML);
                        }
                    }
                    else {
                        let imageHTML = '<img src="' + image.name + '" alt ="' + image.title + '"></img>';
                        let imageString = '%{Image(' + iteratorImages + ')}';
                        answer.text = answer.text.replace(imageString, imageHTML);
                    }
                }
            }
        }
    }
}
